const gameContainer = document.getElementById("game");
let level=localStorage.getItem('level');
console.log(level);
let COLORS=[];
var modal = document.getElementById("myModal");
modal.style.display="none";
if(level==='easy'){
  COLORS=[
  "1.gif",
  "2.gif",
  "3.gif",
  "1.gif",
  "2.gif",
  "3.gif",
  ]
}
else if(level==='medium'){
  COLORS=[
    "1.gif",
    "2.gif",
    "3.gif",
    "1.gif",
    "2.gif",
    "3.gif",
    "6.gif",
    "6.gif"
    ]
}
else{
  COLORS = [
    "1.gif",
    "2.gif",
    "3.gif",
    "4.gif",
    "5.gif",
    "2.gif",
    "1.gif",
    "5.gif",
    "3.gif",
    "4.gif",
    "6.gif",
    "6.gif"
  ];
}


// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}
function restart() {
  let button=document.getElementById("restart")
  button.addEventListener("click",(event)=>{
    location.reload()
  })
}
let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
   
    newDiv.classList.add(color);
    newDiv.style.backgroundImage="none"
    newDiv.style.backgroundPosition="center"
    newDiv.style.backgroundRepeat="no-repeat"
    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
    // if(colorArray.indexOf(color)==Math.floor(colorArray.length/2)){
    //   const break=document.createElement('br');
    //   gameContainer.append(break);
    // }
      
  }
}

// TODO: Implement this function!
// you can use event.target to see which element was clicked
let count=0
let firstClick=0
let secondClick=0
colorsArr=[]
let clicks=0

function handleCardClick(event) {
    count++;
    click=Math.floor(count/2);
    document.getElementById("number").innerText=click
    let color=event.target.classList[0]
    if(firstClick===0){
      firstClick=event
      event.target.style.backgroundImage=`url(gifs/${event.target.classList[0]})`
      event.target.style.backgroundPosition="center"
      event.target.style.backgroundRepeat="no-repeat"
      event.target.classList.add("active")
    }
    else{
      if(secondClick===0){
        secondClick=event
        if(!secondClick.target.classList.contains('active')){
          if(firstClick.target.classList[0]==secondClick.target.classList[0]){
            secondClick.target.style.backgroundImage=`url(gifs/${secondClick.target.classList[0]})`
            secondClick.target.style.backgroundPosition="center"
            secondClick.target.style.backgroundRepeat="no-repeat"
            secondClick.target.classList.add('active')
            firstClick.target.style.pointerEvents="none"
            secondClick.target.style.pointerEvents="none"
            colorsArr.push(firstClick.target.classList[0])
            if(colorsArr.length===Math.floor(COLORS.length/2)){
              if(localStorage.getItem("HighScore")==undefined){
                localStorage.setItem("HighScore",click)
              }
              if(click<localStorage.getItem("HighScore")){
                localStorage.setItem("HighScore",click)
              }
              setTimeout(()=>{
                
                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];
                
                document.getElementById('num').innerText=click;
                modal.style.display = "block";


                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                  modal.style.display = "none";
                }

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                  if (event.target == modal) {
                    modal.style.display = "none";
                  }
                }

              },1000)
            }
            firstClick=0
            secondClick=0
          }
          else{
            secondClick.target.style.backgroundImage=`url(gifs/${secondClick.target.classList[0]})`
            setTimeout(()=>{
              firstClick.target.style.backgroundImage="none"
              secondClick.target.style.backgroundImage="none"
              firstClick.target.style.backgroundPosition="center"
              secondClick.target.style.backgroundPosition="center"
              firstClick.target.style.backgroundRepeat="no-repeat"
              secondClick.target.style.backgroundRepeat="no-repeat"
              firstClick.target.classList.remove("active")
              secondClick.target.classList.remove("active")
              firstClick=0
              secondClick=0
            },1000)
          }
        }
        else{
          firstClick=0
          secondClick=0
        }
      }
    }
}



function HighScore(){
  let score=document.getElementById("score")
  score.innerText=localStorage.getItem("HighScore")
}

createDivsForColors(shuffledColors);
restart()
HighScore()
